import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.LinkedList;

@XmlElement
public class Student {

    @XmlAttribute
    private String firstname;
    @XmlAttribute
    private String lastname;
    @XmlAttribute
    private String groupnumber;

    private LinkedList<Subject> subject;

    private float average;

    Student(String firstname, String lastname, String groupnumber, LinkedList<Subject> subject, float average) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.groupnumber = groupnumber;
        this.setSubject(subject);
        this.setAverage(average);
    }

    Student() {

    }


    public LinkedList<Subject> getSubject() {
        return subject;
    }

    public void setSubject(LinkedList<Subject> subject) {
        this.subject = subject;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getGroupnumber() {
        return groupnumber;
    }

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }
}
