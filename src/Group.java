import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;

@XmlRootElement
public class Group {

    private LinkedList<Student> student;

    Group(LinkedList<Student> student) {
        this.setStudent(student);
    }

    Group(){

    }

    public LinkedList<Student> getStudent() {
        return student;
    }

    public void setStudent(LinkedList<Student> student) {
        this.student = student;
    }
}
