import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import java.io.*;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) throws JAXBException, IOException, ParserConfigurationException, SAXException {

        String inputFile = "group.xml";
        String outputFile = "groupOut.xml";

        checkOfAverage(inputFile, outputFile);

    }

    private static void checkOfAverage(String inputFile, String outputFile) throws JAXBException, ParserConfigurationException, SAXException, FileNotFoundException {
        float sumMark = 0, numMark = 0;
        float averageMark = 0;

        JAXBContext jc = JAXBContext.newInstance(Group.class);

        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        parserFactory.setValidating(false);
        SAXParser saxParser = parserFactory.newSAXParser();
        XMLReader xmlReader = saxParser.getXMLReader();
        EntityResolver entityResolver = new EntityResolver() {
            @Override
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                return new InputSource(new StringReader(""));
            }
        };

        SAXSource saxSource = new SAXSource(xmlReader, new InputSource(inputFile));
        xmlReader.setEntityResolver(entityResolver);
        Unmarshaller um = jc.createUnmarshaller();
        Group group = (Group) um.unmarshal(saxSource);

        LinkedList<Student> umStudents = group.getStudent();

        for (Student student : umStudents) {
            for (Subject subject : student.getSubject()) {
                sumMark += subject.getMark();
                numMark++;
            }
            averageMark = sumMark / numMark;
            if (student.getAverage() != averageMark) student.setAverage(averageMark);
            sumMark = 0;
            numMark = 0;
            averageMark = 0;
        }

        Marshaller m = jc.createMarshaller();

        OutputStream outputStream = new FileOutputStream(outputFile);
        m.marshal(group, outputStream);
    }
}