import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAttribute;

@XmlElement
public class Subject {

    @XmlAttribute
    private String title;
    @XmlAttribute
    private int mark;

    Subject(String title, int mark) {

        this.title = title;
        this.mark = mark;

    }

    Subject() {

    }


    public String getTitle() {
        return title;
    }

    public int getMark() {
        return mark;
    }
}
